import 'package:flutter/material.dart';
import 'package:widget_arrows/widget_arrows.dart';
import 'clause.dart';
import 'globals.dart' as globals;

// ignore: must_be_immutable
class ToolScreen extends StatefulWidget {
  static const routename = '/tool';

  static ToolScreenState of(BuildContext context) =>
      context.findAncestorStateOfType<ToolScreenState>();

  @override
  ToolScreenState createState() => ToolScreenState();
}

class ToolScreenState extends State<ToolScreen> {
  //List<Position> pos = List<Position>();
  List<Clause> movableItems = [];
  List<Clause> initialClauses;
  bool _complete = false;

  bool showToolTips = false;
  void addClause(Clause c) {
    setState(() {
      movableItems.add(c);
    });
  }

  /*
   * Author: Christopher Belshaw
   * Purpose: To be completed
  */
  void showError(String errorText) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(errorText,
            textAlign: TextAlign.center,
            textScaleFactor: 2,
            style: TextStyle(color: Colors.red))));
  }

  /*
   * Author: Christopher Belshaw
   * Purpose: To be completed
  */
  @override
  initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      setState(() {
        initialClauses = ModalRoute.of(context).settings.arguments;
      });

      if (initialClauses != null) {
        for (Clause c in initialClauses) {
          movableItems.add(c);
        }
      }
      globals.toolscreenState = this;
    });
  }

  /*
   * Author: Christopher Belshaw
   * Purpose: To be completed
  */
  void _reset() {
    //Navigator.of(context).pushNamed('/tool', arguments: initialClauses);
    double offset = 20;

    for (Clause c in initialClauses) {
      c.xPosition = offset += 120;
      c.yPosition = 30;
    }

    Navigator.pushReplacementNamed(context, '/tool', arguments: initialClauses);
  }

  /*
   * Authors: Suchismita Kundu, Christopher Belshaw
   * Purpose: Builds the tool interface where clause objects can be dragged,
   * builds buttons for the user to reset the tool, ask for hints, and submit
   * the diagram built for checking. Includes dynamic congratulatory widget
   * when the user reached the empty set.
  */
  @override
  Widget build(BuildContext context) => ArrowContainer(
          child: Scaffold(
        body: Stack(
          children: <Widget>[
            Image(
              image: AssetImage("assets/images/ResolutoolBG.png"),
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              fit: BoxFit.cover,
            ),
            Column(
              children: <Widget>[
                Container(
                  child: Stack(
                    children: <Widget>[
                      Scaffold(
                        backgroundColor: Colors.transparent,
                        appBar: new AppBar(
                          title: Text('RESOLUTOOL',
                              style: TextStyle(color: Colors.white)),
                          backgroundColor:
                              Colors.deepPurple[800].withOpacity(0.2),
                        ),
                      ),
                    ],
                  ),
                  width: MediaQuery.of(context).size.width,
                  height: 50,
                  color: Colors.transparent,
                ),
                Container(
                  child: Stack(
                    fit: StackFit.loose,
                    children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height - 50,
                            color: Colors.transparent,
                          ),
                        ] +
                        movableItems,
                  ),
                ),
              ],
            ),
            AnimatedOpacity(
                opacity: _complete ? 1.0 : 0.0,
                duration: Duration(milliseconds: 250),
                child: (_complete)
                    ? Align(
                        // ----------
                        alignment: Alignment(0.0, 0.0),
                        child: Container(
                          width: 460,
                          height: 290,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Colors.white.withOpacity(0.7),
                          ),
                          child: Column(
                            children: [
                              Align(
                                alignment: Alignment.topRight,
                                child: new IconButton(
                                  onPressed: () {
                                    setState(() {
                                      _complete = !_complete;
                                    });
                                  },
                                  icon: Icon(Icons.close_rounded,
                                      color: Colors.deepPurple[300]),
                                  tooltip: 'Exit',
                                ),
                              ),
                              Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 15.0)),
                              Text(
                                'Congratulations! \n You resolved the empty set!',
                                style: TextStyle(
                                    fontSize: 22,
                                    fontWeight: FontWeight.normal),
                                textAlign: TextAlign.center,
                              ),
                              Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 8.0)),
                              Text(
                                'You did it in ' +
                                    (((movableItems == null)
                                                ? 0
                                                : movableItems.length) -
                                            ((initialClauses == null)
                                                ? 0
                                                : initialClauses.length) -
                                            1)
                                        .toString() +
                                    ((((movableItems == null)
                                                    ? 0
                                                    : movableItems.length) -
                                                ((initialClauses == null)
                                                    ? 0
                                                    : initialClauses.length) -
                                                1 ==
                                            1)
                                        ? ' clause.'
                                        : ' clauses.'),
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.normal),
                                textAlign: TextAlign.center,
                              ),
                              Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 15.0)),
                              new ButtonBar(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  //Button inputs the '{' symbol
                                  new FloatingActionButton.extended(
                                    heroTag: "refresh",
                                    onPressed: () {
                                      _reset();
                                    },
                                    label: Text('Reset',
                                        style: TextStyle(color: Colors.white)),
                                    icon: Icon(Icons.refresh,
                                        color: Colors.white),
                                    backgroundColor: Colors.deepPurple[300],
                                    tooltip: 'Restart the diagram',
                                  ),
                                  new FloatingActionButton.extended(
                                    heroTag: "home",
                                    onPressed: () {
                                      Navigator.of(context).pushNamed('/home');
                                    },
                                    label: Text('Home',
                                        style: TextStyle(color: Colors.white)),
                                    icon: Icon(Icons.home, color: Colors.white),
                                    backgroundColor: Colors.deepPurple[300],
                                    tooltip: 'Resolve a different formula',
                                  ),
                                  new FloatingActionButton.extended(
                                    heroTag: "back",
                                    onPressed: () {
                                      setState(() {
                                        _complete = !_complete;
                                      });
                                    },
                                    label: Text('Back',
                                        style: TextStyle(color: Colors.white)),
                                    icon: Icon(Icons.exit_to_app_rounded,
                                        color: Colors.white),
                                    tooltip: 'Return to board',
                                    backgroundColor: Colors.deepPurple[300],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      )
                    : null), //-------

            Align(
              alignment: Alignment(0.95, -0.74),
              child: new FloatingActionButton.extended(
                heroTag: "reset",
                onPressed: () {
                  _reset();
                },
                label: Text(
                  'Reset',
                  style: TextStyle(color: Colors.deepPurple[400]),
                ),
                icon: Icon(
                  Icons.refresh,
                  color: Colors.deepPurple[400],
                ),
                backgroundColor: Colors.white.withOpacity(0.7),
              ),
            ),
            Align(
              alignment: Alignment(0.95, -0.55),
              child: new FloatingActionButton.extended(
                tooltip: (showToolTips) ? "ToolTips are on!" : "Show tooltips?",
                heroTag: "tooltips",
                onPressed: () {
                  setState(() {
                    this.showToolTips = !this.showToolTips;
                  });
                },
                label: Text(
                  'Tooltips',
                  style: TextStyle(color: Colors.deepPurple[400]),
                ),
                icon: (showToolTips)
                    ? Icon(
                        Icons.check,
                        color: Colors.green[400],
                      )
                    : Icon(
                        Icons.help,
                        color: Colors.deepPurple[400],
                      ),
                backgroundColor: Colors.white.withOpacity(0.7),
              ),
            ),
            Align(
              alignment: Alignment(0.95, 0.95),
              child: new FloatingActionButton.extended(
                heroTag: "Check",
                onPressed: () {
                  for (Clause c in movableItems) {
                    if (c.literals.isEmpty) {
                      print("Found the empty set!");
                      setState(() {
                        _complete = true;
                      });
                      break;
                    }
                  }
                },
                label: Text(
                  'Check',
                  style: TextStyle(color: Colors.deepPurple[400]),
                ),
                icon: Icon(
                  Icons.sticky_note_2_outlined,
                  color: Colors.deepPurple[400],
                ),
                backgroundColor: Colors.white.withOpacity(0.7),
              ),
            ),
          ],
        ),
      ));
}
