import 'package:flutter/material.dart';
import 'package:resolutool/toolscreen.dart';
import 'clause.dart';
import 'logic.dart';
import 'toolscreen.dart';

void main() => runApp(ResolutoolApp());

class ResolutoolApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'ResoluTool',
      theme: ThemeData(
        // This is the theme of our application.
        primarySwatch: Colors.orange,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
        appBarTheme: AppBarTheme(
          color: Colors.deepPurple[800].withOpacity(0.2),
        ),
      ),
      home: WelcomeScreen(title: 'RESOLUTOOL'),
      routes: {
        '/tool': (context) => ToolScreen(),
        '/home': (context) => WelcomeScreen(title: 'RESOLUTOOL'),
      },
    );
  }
}

class WelcomeScreen extends StatelessWidget {
  WelcomeScreen({Key key, this.title}) : super(key: key);
  final String title;

  /*
    Author: Suchismita Kundu
    Purpose: Initial formatting of home page and app theme.
  */
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        Image(
          image: AssetImage("assets/images/ResolutoolBG.png"),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          fit: BoxFit.cover,
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: new AppBar(
            // Here we take the value from the MyHomePage object that was created by
            // the App.build method, and use it to set our appbar title.
            title: Text(this.title, style: TextStyle(color: Colors.white)),
          ),
        ),
        Center(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Educational Tool for Proving \n Propositional Formulae by Resolution',
                  style: TextStyle(
                    color: Colors.orange[50].withOpacity(0.9),
                    fontSize: 42,
                    fontWeight: FontWeight.w400,
                    letterSpacing: 0.9,
                    wordSpacing: 4.0,
                    shadows: [
                      Shadow(
                        color: Colors.purple[300],
                        blurRadius: 7.0,
                        offset: Offset(5.0, 5.0),
                      ),
                      Shadow(
                        color: Colors.orange[200],
                        blurRadius: 10.0,
                        offset: Offset(-4.0, 5.0),
                      ),
                    ],
                  ),
                  textAlign: TextAlign.center,
                ),
                Padding(padding: const EdgeInsets.symmetric(vertical: 18.0)),
                SizedBox(
                  width: 900,
                  height: 270,
                  child: Card(
                    elevation: 11,
                    color: Colors.orange[50].withOpacity(0.6),
                    child: EnterForm(),
                  ),
                ),
              ]),
        ),
      ],
    ));
  }
}

class EnterForm extends StatefulWidget {
  @override
  _EnterFormState createState() => _EnterFormState();
}

class _EnterFormState extends State<EnterForm> {
  /*
    Author: Frankie Cleary
    Purpose: This method is called on initialisation of the state, and adds a listener for the controller
  */
  final inputController = TextEditingController();
  //text: '{¬a, b}{¬b, c, a}{¬c, b}{¬b}{c}'
  void initState() {
    super.initState();
  }

  /*
    Author: Suchismita Kundu
    Purpose: A method to track form progress, i.e. make sure appropriate requirements
    are fulfilled in the textFormField, using error checks, before the resolve button can be clicked.
  */
  double _formProgress = 0;
  // bool _andOrError = false;
  // bool _intError = false;
  // bool _bracError = false;
  void _updateFormProgress() {
    var progress = 0.0;
    bool andOrError = false;
    bool intError = false;
    bool bracError = false;

    String match = inputController.value.text;
    int left = '{'.allMatches(match).length;
    int right = '}'.allMatches(match).length;
    var controllers = [inputController];
    if (match.contains('∧') || match.contains('∨')) {
      andOrError = true;
      progress = 0 / controllers.length;
    }
    if (match.contains(new RegExp(r'[0-9]'))) {
      intError = true;
      progress = 0 / controllers.length;
    }
    if ((match.isNotEmpty) && (left < 1 || left != right)) {
      bracError = true;
      progress = 0 / controllers.length;
    } else if (match.isNotEmpty) {
      if (match.length < 100) {
        if (left == right && left > 0) {
          progress += 1 / controllers.length;
        }
      }
    }

    setState(() {
      _formProgress = progress;
      // _andOrError = andOrError;
      // _intError = intError;
      // _bracError = bracError;
    });
  }

  /*
    Author: Frankie Cleary
    Purpose: This method is called to add the selected symbol to the string,
    called when a symbol button is pressed
    @param symbol   This is the symbol of the button that was pressed,
     passed via text and appended to the string
   */
  _addSymbol(String symbol) {
    setState(() {
      int cursorPos = inputController.selection.base.offset;
      inputController.value = inputController.value.copyWith(
          text: inputController.text.replaceRange(cursorPos, cursorPos, symbol),
          selection:
              TextSelection.fromPosition(TextPosition(offset: cursorPos + 1)));
    });
  }

  /*
    Author: Frankie Cleary, Chris Belshaw, Suchismita Kundu
    Purpose: This method is provided with the input string,
    it then proceeds to split it into its literals, and solve it.
    If the formula is solvable, the user is redirected to the tool interface.
    @param input  The input from the textfield, within  which a user enters this clausal form statement
   */
  void resolveCheck(String input) {
    if (solvable(input)) {
      Iterable<String> clauseStrings = bracketRemoval(input);
      Map<String, Clause> clauses = new Map<String, Clause>();
      double offset = 20;

      for (String c in clauseStrings) {
        Clause clause = Clause.fromString(c);
        clause.xPosition = offset += 120;
        clause.yPosition = 30;
        clauses[c] = clause;
      }

      List<Clause> clausesFromMain =
          clauses.entries.map((e) => e.value).toList();

      Navigator.of(context).pushNamed('/tool', arguments: clausesFromMain);
    } else {
      print("Unsolvable");
    }
  }

  /*
    Author: Frankie Cleary
    Purpose: disposes all information pertaining to the inputController
  */
  void dispose() {
    // Clean up the controller when the widget is removed from the
    // widget tree.
    inputController.dispose();
    super.dispose();
  }

  /*
   * Authors: Suchismita Kundu, Frankie Cleary
   * Purpose: This builds the form where the user inputs their clause set,
   * with the aid of symbol insert buttons, and clicks the 'Resolve' button
   * to be redirected to the tool interface.
  */
  @override
  Widget build(BuildContext context) {
    return Form(
      onChanged: _updateFormProgress,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          AnimatedProgressIndicator(value: _formProgress),
          Padding(padding: const EdgeInsets.symmetric(vertical: 20.0)),
          Text(
            'Enter the formula you want to resolve in Clausal Normal Form:',
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.normal),
            textAlign: TextAlign.center,
          ),
          Padding(padding: const EdgeInsets.symmetric(vertical: 5.0)),
          new Row(
            children: <Widget>[
              new Flexible(
                child: new Padding(
                  padding: EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: inputController,
                    decoration: InputDecoration(
                      hintText:
                          'e.g.: {{p, q, ¬p, r}, {p, q, ¬q}, {p, q, ¬q, ¬r}}',
                      filled: true,
                      fillColor: Colors.white,
                      border: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Colors.deepPurple, width: 3.0),
                      ),
                      suffixIcon: IconButton(
                        onPressed: () => inputController.clear(),
                        icon: Icon(Icons.clear),
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                width: 100.0,
                height: 50.0,
                child: TextButton(
                  style: ButtonStyle(
                    foregroundColor: MaterialStateColor.resolveWith(
                        (Set<MaterialState> states) {
                      return states.contains(MaterialState.disabled)
                          ? null
                          : Colors.white;
                    }),
                    backgroundColor: MaterialStateColor.resolveWith(
                        (Set<MaterialState> states) {
                      return states.contains(MaterialState.disabled)
                          ? null
                          : Colors.orange[400];
                    }),
                  ),
                  onPressed: () => {resolveCheck(inputController.text)},
                  //onPressed: () => {chrisTesting(inputController.text)},
                  child: Text(
                    'Resolve',
                    style:
                        TextStyle(fontSize: 18, fontWeight: FontWeight.normal),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Padding(padding: EdgeInsets.all(8.0)),
            ],
          ),
          Text(
            "Type an apostrophe (') to enter the not symbol (¬), or use the buttons below:",
            style: TextStyle(fontSize: 13, color: Colors.deepPurple[500]),
          ),
          Padding(
              padding: EdgeInsets.all(1),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(1),
                    /*
                     * Authors: Suchismita Kundu, Samuel Roffey
                     * Purpose: Inserts a button bar that allows user to input logic symbols aided by _addSymbol().
                     */
                    child: new ButtonBar(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        //Button inputs the '{' symbol
                        Container(
                          width: 38.0,
                          height: 35.0,
                          child: RaisedButton(
                            child: new Text(
                              '{',
                              style: TextStyle(fontSize: 16),
                            ),
                            color: Colors.deepPurple[300],
                            onPressed: () {
                              _addSymbol("{");
                            },
                          ),
                          //Button inputs the '{' symbol
                        ),

                        Container(
                          width: 38.0,
                          height: 35.0,
                          child: RaisedButton(
                            child: Text(
                              ' }',
                              style: TextStyle(fontSize: 16),
                            ),
                            color: Colors.deepPurple[300],
                            onPressed: () {
                              _addSymbol("}");
                            },
                          ),
                          //Button inputs the '}' symbol
                        ),
                        Container(
                          width: 38.0,
                          height: 35.0,
                          child: RaisedButton(
                            child: Text(
                              '¬',
                              style: TextStyle(fontSize: 16),
                            ),
                            color: Colors.deepPurple[300],
                            onPressed: () {
                              _addSymbol("¬");
                            },
                          ),
                          //Button inputs the '¬' symbol
                        ),
                      ],
                    ),
                  ),
                ],
              )),
        ],
      ),
    );
  }
}

/*
   * Authors: Suchismita Kundu
   * Purpose: This class provides a visual indicator of the progress in requirements
   * of the clause input fulfilled by the user, using the error checks in _updateFormProgress().
  */
class AnimatedProgressIndicator extends StatefulWidget {
  final double value;

  AnimatedProgressIndicator({
    @required this.value,
  });

  @override
  State<StatefulWidget> createState() {
    return _AnimatedProgressIndicatorState();
  }
}

class _AnimatedProgressIndicatorState extends State<AnimatedProgressIndicator>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation<Color> _colorAnimation;
  Animation<double> _curveAnimation;

  void initState() {
    super.initState();
    _controller = AnimationController(
        duration: Duration(milliseconds: 1200), vsync: this);

    var colorTween = TweenSequence([
      TweenSequenceItem(
        tween: ColorTween(begin: Colors.orange, end: Colors.yellow),
        weight: 1,
      ),
      TweenSequenceItem(
        tween: ColorTween(begin: Colors.yellow, end: Colors.green),
        weight: 1,
      ),
      TweenSequenceItem(
        tween: ColorTween(begin: Colors.green, end: Colors.deepPurple[400]),
        weight: 1,
      ),
    ]);

    _colorAnimation = _controller.drive(colorTween);
    _curveAnimation = _controller.drive(CurveTween(curve: Curves.easeIn));
  }

  void didUpdateWidget(oldWidget) {
    super.didUpdateWidget(oldWidget);
    _controller.animateTo(widget.value);
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _controller,
      builder: (context, child) => LinearProgressIndicator(
        value: _curveAnimation.value,
        valueColor: _colorAnimation,
        backgroundColor: _colorAnimation.value.withOpacity(0.4),
      ),
    );
  }
}
