/* Author: Callum Hart
 * Literal objects are stored in a list in clause objects so they can referenced
 * and compared when resolving clauses.
 */

class Literal {
  String name;
  bool not;

  Literal(var literalString) {
    // Takes literal string generated by clause class as input
    name = literalString.replaceAll(new RegExp(r"¬"),
        ""); // name of literal is same as literal just without any "¬" symbols
    if (literalString.contains("¬")) {
      // Set the "not" boolean to true if literal string contains "¬", so we know which literals have negation
      not = true;
    } else {
      not = false;
    }
  }

  Literal.direct(String literalName, bool negation) {
    this.name = literalName;
    this.not = negation;
  }

  Literal.inverse(Literal target) {
    name = target.name;
    not = !target.not;
  }

  bool equals(Literal b) {
    if ((this.name == b.name) && (this.not == b.not)) {
      return true;
    } else {
      return false;
    }
  }
}
