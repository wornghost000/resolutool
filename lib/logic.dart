import 'dart:math';
import 'clause.dart';
import 'literal.dart';
import 'globals.dart' as globals;

/*
    Author: Frankie Cleary, Callum Hart
    Purpose: This method is provided with the input string,
    the it just removes brackets and cleans out empty elements,
    it returns a list of clause strings
    @param input The input from the user, taken from another method that may call this
*/
Iterable<String> bracketRemoval(String input) {
  RegExp bracketRemoval = new RegExp(
      r"( )*((\{|\}|(\(|\)))( )*(,)?( )*)"); // regex to remove brackets and clean
  List<String> splitInput = input.split(bracketRemoval);
  Iterable<String> filteredString = splitInput.where((x) {
    x = x.trimLeft();
    x = x.trimRight();
    if (x.trim().length == 0) {
      return false;
    } else {
      return x.contains(new RegExp(r".")); //return if x contains characters.
    }
  });

  return filteredString;
}

/*
    Author: Frankie Cleary
    Purpose: This method is provided with the input string,
    it then proceeds to split it into its literals, and will proceed to solve it
    @param input  The input from the textfield, within  which a user enters this clausal form statement
 */
bool solvable(String input) {
  /*
      This section cleans the input, ready for solving afterwards
     */
  var literals = new Map();
  RegExp literalCreator = new RegExp(r"(,| )"); //regex named for purpose

  var clauseStrings = bracketRemoval(input);
  //This section takes all clauses and turns them into literals with no negation symbols involved
  clauseStrings.forEach((x) {
    var literalList = x.split(
        literalCreator); //create a list of literals, however the list includes negation symbols and spaces
    literalList.forEach((y) {
      //iterate the list created by the splitter, in order to clean the input
      if (y != "") {
        y = y.replaceAll(new RegExp(r'¬'),
            ''); //replace all negation symbols with blank space for purposes of finding literals
        literals[y] =
            false; //Create a new pairing, with the literal as a key and a boolean as its partner
        //as a result of using the literal as the key, you do not get repeat literals
      }
    });
  });

  // this section allows you to iterate through every combination of true/false literals
  for (var x = 0; x < pow(2, literals.length); x++) {
    int booleanCounter = x;
    double currentNum = pow(2, literals.length - 1).toDouble();
    for (var k in literals.keys) {
      if (booleanCounter >= currentNum) {
        literals[k] = true;
        booleanCounter -= currentNum.toInt();
      } else {
        literals[k] = false;
      }
      currentNum /= 2;
    }

    //actually do the checks
    List<String> clauseTrueFalse = List.filled(clauseStrings.length, "");
    int count = 0;
    clauseStrings.forEach((i) {
      var literalList = i.split(
          literalCreator); //create a list of literals, however the list includes negation symbols and spaces
      String temp = "";
      literalList.forEach((j) {
        //iterate the list created by the splitter, in order to clean the input
        String clause = " ";
        literals.forEach((k, v) {
          if ((j != "" && j != " ") && j.contains(k)) {
            if (j.contains('¬') && v == false) {
              clause += "1";
            } else if (j.contains('¬') && v == true) {
              clause += "0";
            } else if (v == false) {
              clause = "0";
            } else if (v == true) {
              clause = "1";
            }
          }
        });
        temp += clause;
      });
      clauseTrueFalse[count] = temp;
      count = count + 1;
    });
    var innerfail = false;
    clauseTrueFalse.forEach((e) {
      if (!e.contains("1")) {
        innerfail = true;
      }
    });
    if (!innerfail) {
      return false;
    }
  }
  return true;
}

/*
  Author: Callum Hart, Chris Belshaw
  Purpose: Generates clause objects, each with a list of literal objects from the user's inputted string, returns
  list of these clause objects
  @param: clauseStrings The split input string of clauses made from the users input
 */
List<Clause> generateClauses(Iterable clauseStrings) {
  List<Clause> clauses = [];
  var name = 1;
  // Generate clause objects from list of clause strings taken from input string
  clauseStrings.forEach((x) {
    Clause clause = Clause.fromString(x);
    clause.name = name++;
    clauses.add(clause);
  });
  return clauses;
}

/*
  Author: Callum Hart
  Purpose: Generates new clause objects and literal objects from original list of clauses
  in order to resolve the user's inputted logic
   */
void solve(List<Clause> clauses) {
  var linkMap = new Map();
  List<Clause> newClauses;
  var name = clauses.length + 1;
  bool isEmpty = false;
  outerloop:
  for (var x in clauses) {
    // Compares clauses in order to generate new clauses
    firstloop:
    for (var y in clauses) {
      // Checks each clause against every other clause
      if (x == y) {
        continue firstloop;
      }
      for (var l in linkMap.values) {
        if (l.contains(x.name) && l.contains(y.name)) {
          continue firstloop;
        }
      }
      for (var a in x.literals) {
        for (var b in y.literals) {
          if (a.name == b.name && a.not != b.not) {
            var literals = x.literals + y.literals;
            literals.remove(a);
            literals.remove(b);
            for (var xl in x.literals) {
              for (var yl in y.literals) {
                if (xl.name == yl.name && xl.not == yl.not) {
                  literals.remove(xl);
                }
              }
            }
            Clause newClause = new Clause.logic(name);
            for (var c in clauses) {
              if (c.literals.length == newClause.literals.length &&
                  c.literals.every(newClause.literals.contains)) {
                continue firstloop;
              }
            }
            List link;
            link.add(x.name);
            link.add(y.name);
            linkMap[newClause.name] = link;
            name++;
            if (literals.isEmpty) {
              newClause.literals.add(new Literal("∅"));
              newClauses.add(newClause);
              isEmpty = true;
              break outerloop;
            } else {
              literals.forEach((l) => newClause.literals.add(l));
              newClauses.add(newClause);
            }
            continue firstloop;
          }
        }
      }
    }
  }
  clauses += newClauses;
  if (!isEmpty) {
    solve(clauses);
  } else {
    clauses.forEach((x) {
      //print(x.name.toString() + ":");
      x.literals.forEach((y) {
        //print(y.name.toString() + " " + y.not.toString());
      });
    });
  }
}

/// @author Chris, Callum
List<Clause> compareClauses(Clause c1, c2) {
  if (c1 == c2) {
    globals.toolscreenState.showError("Can't combine a clause with itself.");
    return null;
  } else {
    // When a clause is compared, the different pairs of literals can be removed.
    List<Literal> c1Literals = c1.literals;
    List<Literal> c2Literals = c2.literals;
    List<Literal> combinedLiterals = c1Literals + c2Literals;
    combinedLiterals = dedupe(combinedLiterals);
    combinedLiterals = alphabeticalSort(combinedLiterals);
    List<Clause> outputClauses = [];

    for (Literal l1 in c1Literals) {
      for (Literal l2 in c2Literals) {
        List<Literal> workingLiterals = combinedLiterals.toList();
        // if the literal in the second clause is the inverse of the literal in the first clause.
        if (l1.equals(Literal.inverse(l2))) {
          workingLiterals.remove(l1);
          workingLiterals.remove(l2);
          Clause outputClause = new Clause.fromLiterals(workingLiterals);
          outputClause.parents = [c1, c2];
          outputClauses.add(outputClause);
        }
      }
    }
    List<Clause> alreadyPresent = globals.toolscreenState.movableItems;
    List<Clause> output = outputClauses;
    for (Clause cprime in alreadyPresent) {
      for (Clause c in outputClauses) {
        if (c.equals(cprime)) {
          output.remove(c);
        }
      }
    }
    for (Clause c in output) {
      c.xPosition = (c.parents[0].xPosition + c.parents[1].xPosition) / 2;
      c.yPosition = (c.parents[0].yPosition > c.parents[1].yPosition)
          ? c.parents[0].yPosition + 100
          : c.parents[1].yPosition + 100;
    }
    return output;
  }
}

List<Literal> dedupe(List<Literal> inputList) {
  Map<String, Literal> out = {};
  for (Literal l in inputList) {
    String reference = l.not.toString() + l.name;
    out[reference] = l;
  }
  List<Literal> filtered = out.values.toList();
  return filtered;
}

List<Literal> alphabeticalSort(List<Literal> inputList) {
  List<Literal> copyList = inputList;
  copyList.sort((a, b) {
    int compare = a.name.compareTo(b.name);

    if (compare == 0) {
      int _a = a.not ? 1 : 0;
      int _b = b.not ? 1 : 0;
      return _a.compareTo(_b);
    } else {
      return compare;
    }
  });
  return copyList;
}
