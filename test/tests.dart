// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.
/*
    Author: Frankie Cleary
    Purpose: This class is for tests regarding to the logic section of our tool
 */
import 'package:flutter_test/flutter_test.dart';
import 'package:resolutool/logic.dart';

void main() {
  group('Logic Tests', () {
    group('Bracket tests', () {
      test('Bracket removal works (purely brackets)', () {
        Iterable givenSolution = bracketRemoval("{{}}        }  }");
        Iterable expectedSolution = {};
        expect(givenSolution, expectedSolution);
      });

      test('Bracket removal works (letters, without spaces)', () {
        Iterable givenSolution =
            bracketRemoval("{a{b{c}d}e{f}g{h}i}j}k{l}m}n}o{p{q");
        Iterable expectedSolution = {
          "a",
          "b",
          "c",
          "d",
          "e",
          "f",
          "g",
          "h",
          "i",
          "j",
          "k",
          "l",
          "m",
          "n",
          "o",
          "p",
          "q"
        };
        expect(givenSolution, expectedSolution);
      });

      test('Bracket removal works (letters, with spaces)', () {
        Iterable givenSolution = bracketRemoval("{{a { b}c}d}");
        Iterable expectedSolution = {"a", "b", "c", "d"};
        expect(givenSolution, expectedSolution);
      });
    });

    group('Solvable Tests', () {
      test('Solvable Method Works (should fail)', () {
        bool givenSolution = solvable("{{¬p, q}, {¬q, r}}");
        expect(givenSolution, false);
      });

      test('Solvable Method Works (should fail)', () {
        bool givenSolution = solvable("{{¬p, q}, {¬q, r}, {¬r , p}}");
        expect(givenSolution, false);
      });

      test('Solvable Method Works (should fail)', () {
        bool givenSolution = solvable("{{p, q, r}, {¬p, ¬q, ¬r}}");
        expect(givenSolution, false);
      });

      test('Solvable Method Works (should complete (3rd version))', () {
        bool givenSolution =
            solvable("{{¬p, q}, {¬q, r}, {¬r , p}, {p, q, r}, {¬p, ¬q, ¬r}}");
        expect(givenSolution, true);
      });
    });
  });
}
